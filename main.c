#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <fts.h>
#include <openssl/evp.h>
#include <plist/plist.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#define INFOPLIST_FN "Info.plist"
#define CODERESOURCES_DIR "_CodeSignature"
#define CODERESOURCES_PATH CODERESOURCES_DIR"/CodeResources"

#define LC_SEGMENT 0x1
#define LC_SEGMENT_64 0x19
#define LC_CODE_SIGNATURE 0x1D

struct fat_arch {
  int cputype;
  int cpusubtype;
  uint32_t offset;
  uint32_t size;
  uint32_t align;
};
struct mach_header_64 {
  struct mach_header {
    uint32_t magic;
    int cputype;
    int cpusubtype;
    uint32_t filetype;
    uint32_t ncmds;
    uint32_t sizeofcmds;
    uint32_t flags;
  } mh;
  uint32_t reserved;
};
struct load_command {
  uint32_t cmd;
  uint32_t size;
};
struct segment_command {
  struct load_command lc;
  char segname[16];
  uint32_t vmaddr;
  uint32_t vmsize;
  uint32_t fileoff;
  uint32_t filesize;
  int maxprot;
  int initprot;
  uint32_t nsects;
  uint32_t flags;
  struct section {
    char sectname[16];
    char segname[16];
    uint32_t addr;
    uint32_t size;
    uint32_t offset;
    uint32_t align;
    uint32_t reloff;
    uint32_t nreloc;
    uint32_t flags;
    uint32_t reserved[2];
  } section[];
};
struct segment_command_64 {
  struct load_command lc;
  char segname[16];
  uint64_t vmaddr;
  uint64_t vmsize;
  uint64_t fileoff;
  uint64_t filesize;
  int maxprot;
  int initprot;
  uint32_t nsects;
  uint32_t flags;
  struct section_64 {
    char sectname[16];
    char segname[16];
    uint64_t addr;
    uint64_t size;
    uint32_t offset;
    uint32_t align;
    uint32_t reloff;
    uint32_t nreloc;
    uint32_t flags;
    uint32_t reserved[3];
  } section[];
};
struct linkedit_data_command {
  struct load_command lc;
  uint32_t dataoff;
  uint32_t datasize;
};
struct Blob {
  uint32_t magic;
  uint32_t size;
};
struct SuperBlob {
  struct Blob bh;
  uint32_t count;
  struct BlobIndex {
    uint32_t type;
    uint32_t offset;
  } index[];
};
struct CodeDirectory {
  struct Blob bh;
  uint32_t version;
  uint32_t flags;
  uint32_t hashOffset;
  uint32_t identOffset;
  uint32_t nSpecialSlots;
  uint32_t nCodeSlots;
  uint32_t codeLimit;
  uint8_t hashSize;
  uint8_t hashType;
  uint8_t spare1;
  uint8_t pageSize;
  uint32_t spare2;
  uint32_t scatterOffset;
};
struct signing_context {
  EVP_MD_CTX* mdctx;
  const EVP_MD* md;
  char* entitlements;
  size_t entitlements_size;
  char* bundleID;
  char infoplist_digest[EVP_MAX_MD_SIZE];
  char resources_digest[EVP_MAX_MD_SIZE];
};

static inline uint32_t swap32(uint32_t v,int swap) {
  return swap?__bswap_32(v):v;
}
static inline uint32_t swap64(uint64_t v,int swap) {
  return swap?__bswap_64(v):v;
}
static char* copyBundleID(const char* data,size_t size,char* exefn) {
  plist_t plist=NULL;
  plist_from_memory(data,size,&plist,NULL);
  if(!plist){return NULL;}
  char* bundleID=NULL;
  plist_t node;
  if((!exefn || ((node=plist_dict_get_item(plist,"CFBundleExecutable"))
   && plist_string_val_compare(node,exefn)==0))
   && (node=plist_dict_get_item(plist,"CFBundleIdentifier"))){
    plist_get_string_val(node,&bundleID);
  }
  plist_free(plist);
  return bundleID;
}
static unsigned int digestFile(EVP_MD_CTX* mdctx,const EVP_MD* md,int fd,void* hash) {
  EVP_DigestInit_ex(mdctx,md,NULL);
  while(1){
    char buf[4096];
    ssize_t nread=read(fd,buf,sizeof(buf));
    if(nread<0){return 0;}
    if(!nread){break;}
    EVP_DigestUpdate(mdctx,buf,nread);
  }
  unsigned int mdsize;
  EVP_DigestFinal_ex(mdctx,hash,&mdsize);
  return mdsize;
}
static char* readToEOF(int fd,size_t* sizeptr) {
  struct chunk {
    char buf[4096];
    struct chunk* next;
  } head={0};
  struct chunk* last=&head;
  size_t size=0,i;
  while(1){
    ssize_t nread=read(fd,last->buf,sizeof(head.buf));
    if(nread>0){size+=nread;}
    if(nread<sizeof(head.buf)){break;}
    last=last->next=malloc(sizeof(*last));
  }
  if(!(*sizeptr=size)){return NULL;}
  char* data=malloc(size);
  char* ptr=data;
  struct chunk* cptr=&head;
  while(1){
    struct chunk* cnext=(cptr==last)?NULL:cptr->next;
    memcpy(ptr,cptr->buf,cnext?sizeof(head.buf):data+size-ptr);
    if(cptr!=&head){free(cptr);}
    if(!cnext){break;}
    ptr+=sizeof(head.buf);
    cptr=cnext;
  }
  return data;
}
static uint8_t* readSliceAndSign(FILE* fh,uint32_t* sizeptr,const struct signing_context* context) {
  struct mach_header_64 V_header;
  uint32_t nread=fread(&V_header,1,sizeof(V_header.mh),fh);
  int swap=0,is64=0;
  switch(V_header.mh.magic){
    case 0xCEFAEDFE:swap=1;
    case 0xFEEDFACE:break;
    case 0xCFFAEDFE:swap=1;
    case 0xFEEDFACF:is64=1;
      nread+=fread((char*)&V_header+nread,1,sizeof(V_header)-nread,fh);
      break;
    default:
      fputs("E: Unrecognized header\n",stderr);
      return NULL;
  }
  uint32_t sizeofcmds=swap32(V_header.mh.sizeofcmds,swap);
  uint8_t* lcbuf=malloc(sizeofcmds+sizeof(struct linkedit_data_command));
  nread+=fread(lcbuf,1,sizeofcmds,fh);
  EVP_MD_CTX* mdctx=context->mdctx;
  const EVP_MD* md=context->md;
  const char* bundleID=context->bundleID;
  const char* infoplist_digest=bundleID?context->infoplist_digest:NULL;
  const char* resources_digest=bundleID?context->resources_digest:NULL;
  char* bundleID_new=NULL;
  char infoplist_digest_buf[EVP_MAX_MD_SIZE];
  struct load_command* lcptr=(void*)lcbuf;
  void* lcptr_linkedit=NULL;
  struct linkedit_data_command* lcptr_codesig=NULL;
  uint32_t segcmd=is64?LC_SEGMENT_64:LC_SEGMENT;
  uint32_t ncmds=swap32(V_header.mh.ncmds,swap);
  for (uint32_t i=0;i<ncmds;i++){
    if(lcptr->cmd==segcmd){
      union {
        struct segment_command* ptr;
        struct segment_command_64* ptr64;
      } seg={(void*)lcptr};
      char* segname=is64?seg.ptr->segname:seg.ptr64->segname;
      if(strcmp(segname,"__LINKEDIT")==0){lcptr_linkedit=lcptr;}
      else if(!bundleID && strcmp(segname,"__TEXT")==0){
        uint32_t nsects=swap32(is64?seg.ptr64->nsects:seg.ptr->nsects,swap);
        for (uint32_t j=0;j<nsects;j++){ 
          union {
            struct section* ptr;
            struct section_64* ptr64;
          } sect={is64?(void*)&seg.ptr64->section[j]:(void*)&seg.ptr->section[j]};
          char* sectname=is64?sect.ptr64->sectname:sect.ptr->sectname;
          if(strcmp(sectname,"__info_plist")==0){
            const size_t size=is64?swap64(sect.ptr64->size,swap):
             swap32(sect.ptr->size,swap);
            fpos_t fhpos;
            if(size && fgetpos(fh,&fhpos)==0 && fseek(fh,
             swap32(is64?sect.ptr64->offset:sect.ptr->offset,swap)-nread,SEEK_CUR)==0){
              char* data=malloc(size);
              if(fread(data,size,1,fh) && (bundleID_new=copyBundleID(data,size,NULL))){
                EVP_DigestInit_ex(mdctx,md,NULL);
                EVP_DigestUpdate(mdctx,data,size);
                EVP_DigestFinal_ex(mdctx,infoplist_digest_buf,NULL);
                infoplist_digest=infoplist_digest_buf;
                bundleID=bundleID_new;
              }
              free(data);
              fsetpos(fh,&fhpos);
              if(bundleID){break;} 
            }
          }
        }
      }
    }
    else if(lcptr->cmd==LC_CODE_SIGNATURE){lcptr_codesig=(void*)lcptr;}
    lcptr=(void*)((uint8_t*)lcptr+swap32(lcptr->size,swap));
  }
  uint8_t* slice=NULL;
  if(!lcptr_linkedit){fputs("E: Missing __LINKEDIT segment\n",stderr);}
  else {
    const uint32_t align=1<<4;
    union {
      struct segment_command* ptr;
      struct segment_command_64* ptr64;
    } linkedit={(void*)lcptr_linkedit};
    uint32_t codesize,linkedit_tail,linkedit_size=is64?
     swap64(linkedit.ptr64->filesize,swap):
     swap32(linkedit.ptr->filesize,swap);
    const char* entitlements=context->entitlements;
    uint32_t entitlements_size=context->entitlements_size;
    char* entitlements_new=NULL;
    if(lcptr_codesig){
      codesize=swap32(lcptr_codesig->dataoff,swap);
      linkedit_size-=swap32(lcptr_codesig->datasize,swap);
      linkedit_tail=0;
      if(entitlements_size && !entitlements){
        fpos_t fhpos;
        if(fgetpos(fh,&fhpos)==0 && fseek(fh,codesize-nread,SEEK_CUR)==0){
          size_t nread=0;
          struct SuperBlob V_sigblob;
          if(fread(&V_sigblob,sizeof(V_sigblob),1,fh)
           && V_sigblob.bh.magic==htonl(0xFADE0CC0)){
            nread+=sizeof(V_sigblob);
            const size_t count=ntohl(V_sigblob.count);
            for (uint32_t i=0;i<count;i++){
              struct BlobIndex V_index;
              if(!fread(&V_index,sizeof(V_index),1,fh)){break;}
              nread+=sizeof(V_index);
              if(V_index.type==htonl(0x5)){
                struct Blob V_entblob;
                fseek(fh,ntohl(V_index.offset)-nread,SEEK_CUR);
                if(fread(&V_entblob,sizeof(V_entblob),1,fh)
                 && V_entblob.magic==htonl(0xFADE7171)){
                  const size_t size=ntohl(V_entblob.size)-sizeof(V_entblob);
                  char* data=malloc(size);
                  if(fread(data,size,1,fh)){
                    entitlements=entitlements_new=data;
                    entitlements_size=size;
                  }
                  else {free(data);}
                }
                break;
              }
            }
          }
          fsetpos(fh,&fhpos);
        }
        if(!entitlements && bundleID){
          const char* prexml="<plist version=\"1.0\"><dict>"
           "<key>application-identifier</key><string>";
          const char* postxml="</string></dict></plist>";
          size_t prelen=strlen(prexml),postlen=strlen(postxml),idlen=strlen(bundleID);
          entitlements=entitlements_new=malloc(entitlements_size=prelen+idlen+postlen);
          memcpy((char*)memcpy((char*)memcpy(entitlements_new,prexml,prelen)+prelen,
           bundleID,idlen)+idlen,postxml,postlen);
        }
      }
    }
    else {
      if((linkedit_tail=linkedit_size%align))
        linkedit_size+=linkedit_tail=align-linkedit_tail;
      codesize=(is64?
       swap64(linkedit.ptr64->fileoff,swap):
       swap32(linkedit.ptr->fileoff,swap))+linkedit_size;
      lcptr_codesig=(void*)(lcbuf+sizeofcmds);
      lcptr_codesig->lc.cmd=swap32(LC_CODE_SIGNATURE,swap);
      lcptr_codesig->lc.size=swap32(sizeof(*lcptr_codesig),swap);
      lcptr_codesig->dataoff=swap32(codesize,swap);
      ncmds++;
      sizeofcmds+=sizeof(*lcptr_codesig);
      V_header.mh.ncmds=swap32(ncmds,swap);
      V_header.mh.sizeofcmds=swap32(sizeofcmds,swap);
      if(fseek(fh,sizeof(*lcptr_codesig),SEEK_CUR)==0){nread+=sizeof(*lcptr_codesig);}
    }
    const char* identifier=bundleID?:"";
    const size_t identifier_size=strlen(identifier)+1;
    uint32_t nblobs=entitlements?4:3;
    uint32_t mdsize=EVP_MD_size(md);
    uint32_t nxslots=entitlements?5:resources_digest?3:2;
    const uint32_t ident_cdoff=sizeof(struct CodeDirectory);
    uint32_t codeslots_cdoff=ident_cdoff+identifier_size+nxslots*mdsize;
    const uint8_t pagesize_log2=12;
    const uint32_t pagesize=1<<pagesize_log2;
    uint32_t npages=codesize/pagesize+(codesize%pagesize!=0);
    uint32_t cdblob_size=codeslots_cdoff+npages*mdsize;
    uint32_t cdblob_sigoff=sizeof(struct SuperBlob)+nblobs*sizeof(struct BlobIndex);
    uint32_t reqblob_sigoff=cdblob_sigoff+cdblob_size;
    uint32_t wrapper_sigoff=reqblob_sigoff+sizeof(struct SuperBlob);
    uint32_t entblob_sigoff,entblob_size;
    if(entitlements){
      entblob_sigoff=wrapper_sigoff;
      wrapper_sigoff+=entblob_size=sizeof(struct Blob)+entitlements_size;
    }
    uint32_t sigblob_tail,sigblob_size=wrapper_sigoff+sizeof(struct Blob);
    if((sigblob_tail=sigblob_size%align))
      sigblob_size+=sigblob_tail=align-sigblob_tail;
    lcptr_codesig->datasize=swap32(sigblob_size,swap);
    linkedit_size+=sigblob_size;
    uint32_t linkedit_vmsize=(linkedit_size/pagesize
     +(linkedit_size%pagesize!=0))*pagesize;
    if(is64){
      linkedit.ptr64->filesize=swap64(linkedit_size,swap);
      linkedit.ptr64->vmsize=swap64(linkedit_vmsize,swap);
    }
    else {
      linkedit.ptr->filesize=swap32(linkedit_size,swap);
      linkedit.ptr->vmsize=swap32(linkedit_vmsize,swap);
    }
    size_t header_size=is64?sizeof(V_header):sizeof(V_header.mh);
    slice=malloc(*sizeptr=codesize+sigblob_size);
    uint8_t* ptr=(uint8_t*)memcpy((uint8_t*)memcpy(slice,&V_header,
     header_size)+header_size,lcbuf,sizeofcmds)+sizeofcmds;
    ptr+=fread(ptr,1,codesize-nread-linkedit_tail,fh);
    if(linkedit_tail){
      memset(ptr,0,linkedit_tail);
      ptr+=linkedit_tail;
    }
    if(ptr-slice==codesize){
      struct SuperBlob* sigblob=(void*)ptr;
      sigblob->bh.magic=htonl(0xFADE0CC0);
      sigblob->bh.size=htonl(sigblob_size-=sigblob_tail);
      sigblob->count=htonl(nblobs);
      int i=0;
      sigblob->index[i].type=htonl(0x0);
      sigblob->index[i++].offset=htonl(cdblob_sigoff);
      sigblob->index[i].type=htonl(0x2);
      sigblob->index[i++].offset=htonl(reqblob_sigoff);
      if(entitlements){
        sigblob->index[i].type=htonl(0x5);
        sigblob->index[i++].offset=htonl(entblob_sigoff);
      }
      sigblob->index[i].type=htonl(0x10000);
      sigblob->index[i++].offset=htonl(wrapper_sigoff);
      struct CodeDirectory* cdblob=(void*)((uint8_t*)sigblob+cdblob_sigoff);
      cdblob->bh.magic=htonl(0xFADE0C02);
      cdblob->bh.size=htonl(cdblob_size);
      cdblob->version=htonl(0x20100);
      cdblob->flags=htonl(0x2);
      cdblob->hashOffset=htonl(codeslots_cdoff);
      cdblob->identOffset=htonl(ident_cdoff);
      cdblob->nSpecialSlots=htonl(nxslots);
      cdblob->nCodeSlots=htonl(npages);
      cdblob->codeLimit=htonl(codesize);
      cdblob->hashSize=mdsize;
      cdblob->hashType=1;
      cdblob->pageSize=pagesize_log2;
      cdblob->spare1=cdblob->spare2=cdblob->scatterOffset=0;
      ptr=(uint8_t*)cdblob+ident_cdoff;
      memcpy(ptr,identifier,identifier_size);
      ptr+=identifier_size;
      if(nxslots>4){
        struct Blob* entblob=(void*)((uint8_t*)sigblob+entblob_sigoff);
        entblob->magic=htonl(0xFADE7171);
        entblob->size=htonl(entblob_size);
        memcpy((uint8_t*)entblob+sizeof(*entblob),entitlements,entitlements_size);
        EVP_DigestInit_ex(mdctx,md,NULL);
        EVP_DigestUpdate(mdctx,entblob,entblob_size);
        EVP_DigestFinal_ex(mdctx,ptr,NULL);
        memset(ptr+=mdsize,0,mdsize);
        ptr+=mdsize;
      }
      if(nxslots>2){
        if(resources_digest){memcpy(ptr,resources_digest,mdsize);}
        else {memset(ptr,0,mdsize);}
        ptr+=mdsize;
      }
      struct SuperBlob* reqblob=(void*)((uint8_t*)sigblob+reqblob_sigoff);
      reqblob->bh.magic=htonl(0xFADE0C01);
      reqblob->bh.size=htonl(sizeof(*reqblob));
      reqblob->count=0;
      EVP_DigestInit_ex(mdctx,md,NULL);
      EVP_DigestUpdate(mdctx,reqblob,sizeof(*reqblob));
      EVP_DigestFinal_ex(mdctx,ptr,NULL);
      ptr+=mdsize;
      if(infoplist_digest){memcpy(ptr,infoplist_digest,mdsize);}
      else {memset(ptr,0,mdsize);}
      uint8_t* codeptr=slice;
      uint8_t* codeend=slice+codesize;
      while(codeptr<codeend){
        size_t chunksize=codeend-codeptr;
        if(chunksize>pagesize){chunksize=pagesize;}
        EVP_DigestInit_ex(mdctx,md,NULL);
        EVP_DigestUpdate(mdctx,codeptr,chunksize);
        EVP_DigestFinal_ex(mdctx,ptr+=mdsize,NULL);
        codeptr+=chunksize;
      }
      struct Blob* wrapper=(void*)((uint8_t*)sigblob+wrapper_sigoff);
      wrapper->magic=htonl(0xFADE0B01);
      wrapper->size=htonl(sizeof(*wrapper));
      if(sigblob_tail){memset((uint8_t*)sigblob+sigblob_size,0,sigblob_tail);}
    }
    else {
      fputs("E: fread() failed\n",stderr);
      free(slice);
      slice=NULL;
    }
    free(entitlements_new);
  }
  free(bundleID_new);
  free(lcbuf);
  return slice;
}
int main(int argc,char** argv) {
  char* entpath=NULL;
  int c,overwrite_resources=0;
  while((c=getopt(argc,argv,"fS:"))!=-1){
    switch(c){
      case 'f':overwrite_resources=1;break;
      case 'S':entpath=optarg;break;
      default:fprintf(stderr,"W: Ignoring option -%c\n",optopt);
    }
  }
  char* binpath;
  if(!(optind<argc && (binpath=argv[optind]) && *binpath)){
    fprintf(stderr,"Usage: %s [-f] [-S <entitlements.xml>] <binary>\n",argv[0]);
    return 1;
  }
  FILE* binfh=fopen(binpath,"rb");
  if(!binfh){
    perror(binpath);
    return -1;
  }
  struct signing_context V_context;
  V_context.mdctx=EVP_MD_CTX_create();
  V_context.md=EVP_sha1();
  V_context.bundleID=NULL;
  if(entpath && *entpath){
    int fd;
    if(entpath[0]=='-' && entpath[1]==0){fd=STDIN_FILENO;}
    else if((fd=open(entpath,O_RDONLY))==-1){
      perror(entpath);
      return -1;
    }
    V_context.entitlements=readToEOF(fd,&V_context.entitlements_size);
    if(fd!=STDIN_FILENO){close(fd);}
  }
  else {
    V_context.entitlements_size=entpath?0:1;
    V_context.entitlements=NULL;
  }
  char* slash=strrchr(binpath,'/');
  if(slash){
    *slash=0;
    if(chdir(binpath)){
      perror(binpath);
      return -1;
    }
    binpath=slash+1;
  }
  int infofd=open(INFOPLIST_FN,O_RDONLY);
  if(infofd!=-1){
    size_t size;
    char* data=readToEOF(infofd,&size);
    close(infofd);
    if(data && (V_context.bundleID=copyBundleID(data,size,binpath))){
      EVP_DigestInit_ex(V_context.mdctx,V_context.md,NULL);
      EVP_DigestUpdate(V_context.mdctx,data,size);
      EVP_DigestFinal_ex(V_context.mdctx,V_context.infoplist_digest,NULL);
      int rsfd;
      if(!overwrite_resources && (rsfd=open(CODERESOURCES_PATH,O_RDONLY))!=-1){
        fputs("W: "CODERESOURCES_PATH" exists (use -f to overwrite)\n",stderr);
        if(!digestFile(V_context.mdctx,V_context.md,rsfd,&V_context.resources_digest)){
          perror(CODERESOURCES_PATH);
          return -1;
        }
        close(rsfd);
      }
      else if((mkdir(CODERESOURCES_DIR,0777)==0 || errno==EEXIST)
       && (rsfd=open(CODERESOURCES_PATH,O_WRONLY|O_CREAT|O_TRUNC,0666))!=-1){
        plist_t files=plist_new_dict();
        plist_t files2=plist_new_dict();
        FTS* fts=fts_open((char*const[]){".",NULL},FTS_PHYSICAL,NULL);
        if(fts_read(fts)){
          for (FTSENT* ent=fts_children(fts,0);ent;ent=ent->fts_link){
            const char* fname=ent->fts_name;
            if(strcmp(fname,binpath)==0 || ent->fts_info!=FTS_F){continue;}
            int fd=open(fname,O_RDONLY);
            if(fd!=-1){
              char mdbuf[EVP_MAX_MD_SIZE];
              unsigned int mdsize=digestFile(V_context.mdctx,V_context.md,fd,mdbuf);
              close(fd);
              if(mdsize){
                plist_dict_set_item(files,fname,plist_new_data(mdbuf,mdsize));
                if(strcmp(fname,INFOPLIST_FN)!=0)
                  plist_dict_set_item(files2,fname,plist_new_data(mdbuf,mdsize));
                continue;
              }
            }
            perror(fname);
            return -1;
          }
        }
        fts_close(fts);
        plist_t dict;
        plist_t omit=plist_new_dict();
        plist_dict_set_item(omit,"omit",plist_new_bool(1));
        plist_dict_set_item(omit,"weight",plist_new_real(10));
        plist_t rsplist=plist_new_dict();
        plist_dict_set_item(rsplist,"files",files);
        plist_dict_set_item(rsplist,"files2",files2);
        plist_dict_set_item(rsplist,"rules",dict=plist_new_dict());
        plist_dict_set_item(dict,"^",plist_new_bool(1));
        plist_dict_set_item(dict,"/",plist_copy(omit));
        plist_dict_set_item(rsplist,"rules2",dict=plist_new_dict());
        plist_dict_set_item(dict,"^",plist_new_bool(1));
        plist_dict_set_item(dict,"/",plist_copy(omit));
        plist_dict_set_item(omit,"weight",plist_new_real(20));
        plist_dict_set_item(dict,"^Info\\.plist$",omit);
        char* data=NULL;
        uint32_t size;
        plist_to_bin(rsplist,&data,&size);
        plist_free(rsplist);
        write(rsfd,data,size);
        close(rsfd);
        EVP_DigestInit_ex(V_context.mdctx,V_context.md,NULL);
        EVP_DigestUpdate(V_context.mdctx,data,size);
        EVP_DigestFinal_ex(V_context.mdctx,V_context.resources_digest,NULL);
        free(data);
      }
      else {
        perror(CODERESOURCES_PATH);
        return -1;
      }
    }
    free(data);
  }
  uint32_t v_magic;
  uint32_t offset=fread(&v_magic,1,sizeof(v_magic),binfh);
  if(v_magic==htonl(0xCAFEBABE)){
    uint32_t v_narchs;
    offset+=fread(&v_narchs,1,sizeof(v_narchs),binfh);
    uint32_t narchs=ntohl(v_narchs);
    struct fat_arch* archs=malloc(sizeof(*archs)*narchs);
    offset+=fread(archs,sizeof(*archs),narchs,binfh);
    void** slices=malloc(sizeof(void*)*narchs);
    for (uint32_t i=0;i<narchs;i++){
      fseek(binfh,ntohl(archs[i].offset),SEEK_SET);
      uint32_t size;
      if(!(slices[i]=readSliceAndSign(binfh,&size,&V_context))){return -1;}
      uint32_t align=1<<ntohl(archs[i].align);
      offset=(offset/align+(offset%align!=0))*align;
      archs[i].offset=htonl(offset);
      archs[i].size=htonl(size);
      offset+=size;
    }
    binfh=freopen(NULL,"wb",binfh);
    fwrite(&v_magic,sizeof(v_magic),1,binfh);
    fwrite(&v_narchs,sizeof(v_narchs),1,binfh);
    fwrite(archs,sizeof(*archs),narchs,binfh);
    for (uint32_t i=0;i<narchs;i++){
      fseek(binfh,ntohl(archs[i].offset),SEEK_SET);
      fwrite(slices[i],ntohl(archs[i].size),1,binfh);
      free(slices[i]);
    }
    free(slices);
    free(archs);
  }
  else {
    fseek(binfh,0,SEEK_SET);
    uint32_t size;
    void* slice=readSliceAndSign(binfh,&size,&V_context);
    if(!slice){return -1;}
    binfh=freopen(NULL,"wb",binfh);
    fwrite(slice,size,1,binfh);
    free(slice);
  }
  fclose(binfh);
  EVP_MD_CTX_destroy(V_context.mdctx);
  free(V_context.bundleID);
  free(V_context.entitlements);
}
